@extends('layouts.app')
@section('content')

  <div class="container">
    <div class="row">
      <div class="col-md-10">
        <h3>test</h3>
      </div>
      <div class="col-sm-2">
        <a class="btn btn-sm btn-success" href="{{ route('orderdata.create') }}">Maak een nieuwe order aan</a>
      </div>
    </div>

    @if ($message = Session::get('success'))
      <div class="alert alert-success">
        <p>{{$message}}</p>
      </div>
    @endif

    <table class="table table-hover table-sm">
      <tr>
        <th width = "50px"><b>No.</b></th>
        <th width = "300px">datum en tijd</th>
        <th>totaal_prijs</th>
        <th width = "180px">Action</th>
      </tr>

      @foreach ($orders as $orderdata)
        <tr>
          <td><b>{{++$i}}.</b></td>
          <td>{{$orderdata->datetime}}</td>
          <td>{{$orderdata->total_price}}</td>
          <td>
            <form action="{{ route('orderdata.destroy', $orderdata->id) }}" method="post">
              <a class="btn btn-sm btn-success" href="{{route('orderdata.show',$orderdata->id)}}">Show</a>
              <a class="btn btn-sm btn-warning" href="{{route('orderdata.edit',$orderdata->id)}}">Edit</a>
              @csrf
              @method('DELETE')
              <button type="submit" class="btn btn-sm btn-danger">Delete</button>
            </form>
          </td>
        </tr>
      @endforeach
    </table>

{!! $orders->links() !!}
  </div>
@endsection