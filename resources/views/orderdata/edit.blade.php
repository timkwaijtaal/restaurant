
@extends('layouts.app')
@section('content')
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h3>test</h3>
      </div>
    </div>

    @if ($errors->any())
      <div class="alert alert-danger">
        <strong>Whoops! </strong> there where some problems with your input.<br>
        <ul>
          @foreach ($errors as $error)
            <li>{{$error}}</li>
          @endforeach
        </ul>
      </div>
    @endif

    <form action="{{route('orderdata.update',$orderdata->id)}}" method="post">
      @csrf
      @method('PUT')
      <div class="row">
        <div class="col-md-12">
          <strong>Datum en Tijd :</strong>
          <input type="text" name="datetime" class="form-control" value="{{$orderdata->datetime}}">
        </div>
        <div class="col-md-12">
          <strong>totaal_prijs :</strong>
          <textarea class="form-control" name="total_price" rows="8" cols="80">{{$orderdata->total_price}}</textarea>
        </div>

        <div class="col-md-12">
          <a href="{{route('orderdata.index')}}" class="btn btn-sm btn-success">Back</a>
          <button type="submit" class="btn btn-sm btn-primary">Submit</button>
        </div>
      </div>
    </form>
  </div>
@endsection