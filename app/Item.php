<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public function Sub_soort()
    
    {
        return $this->belongsToMany('App\Sub_soort');
    }

    public function Order_Line()
    
    {
        return $this->belongsToMany('App\Order_Line');
    }

    protected $fillable = [
        'description','sub_soort','price',
        ];
    }
