<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
    
    public function Reserve()
    
    {
        return $this->hasMany('App\Reserve');
    }

    public function Table_Status()
    
    {
        return $this->belongsToMany('App\Table_Status');
    }
}
