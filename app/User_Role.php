<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_Role extends Model
{
    public function Users()
    
    {
        return $this->belongsToMany('App\Users');
    }

    public function Role()
    
    {
        return $this->belongsToMany('App\Role');
    }
}
