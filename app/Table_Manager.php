<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Table_Manager extends Model
{
    public function Order()
    
    {
        return $this->hasMany('App\Order');
    }

    public function Table()
    
    {
        return $this->belongsToMany('App\Table');
    }
}
