<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agenda extends Model
{
    public function Reserve() 
    {
        return $this->hasMany('App/Reserve'); 
    }
}
