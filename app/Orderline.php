<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class Orderline extends Model
{
    public function Item() 
    {
        return $this->belongsToMany('App/Item');
    }

    public function Orderdata() 
    {
        return $this->belongsToMany('App/Orderdata');
    }

    

    protected $fillable = [
        'Order_id','description','amount','price',
        ];
    }
