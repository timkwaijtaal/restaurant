<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserve extends Model

{
    protected $fillable = [
        'Reserve_id',
        'id',
        'Table_id',
        'Agenda_id',
        'datetime',
        'opmerkingen',
        'amount'
    ];

    public function Agenda()  

    {
        return $this->belongsToMany('App\Agenda');
    } 

    public function Table()  

    {
        return $this->belongsToMany('App\Table');
    }

    public function Users()  

    {
        return $this->belongsToMany('App\Users');
    }
        
}