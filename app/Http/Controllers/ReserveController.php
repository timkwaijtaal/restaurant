<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reserve;

class ReserveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

     public function index()
    {
        return view('reserve');
    }
    

    public function create()
    {
        return view('reserves.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'Reserve_id'=>  'required|integer',
            'User_id'=>     'required|integer',
            'Table_id'=>    'required|integer',
            'Agenda_id'=>   'required|integer',
            'datetime'=>    'required',
            'opmerkingen'=> 'required|string',
            'amount' =>     'required|integer'
          ]);
          $reserve = new Reserve([
            'Reserve_id' => $request->post('Reserve_id'),
            'User_id' => $request->post('User_id'),
            'Table_id'=> $request->post('Table_id'),
            'Agenda_id'=> $request->post('Agenda_id'),  
            'datetime' => $request->post('datetime'),
            'opmerkingen'=> $request->post('opmerkingen'),
            'amount'=> $request->post('amount')
          ]);
          $reserve->save();
          return redirect('/reserves')->with('success', 'reserveringen zijn toegevoegd');
        }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
